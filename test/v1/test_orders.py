import copy

from fastapi import status
from starlette.testclient import TestClient
from mock import patch, create_autospec, ANY
import pytest

from orders.v1.orders.models import (NewOrderReq, OrderInDb, OrderStatus, OrderProductsInDb, Orders,
                                     Order, OrderLinks, OrderProducts, OrdersLinks)
from orders.v1.user_model import UserInDb
from orders.v1.products.models import Product, ProductLinks
from orders.api import api
from orders.database import Db


@pytest.fixture
def existing_orders():
    yield [OrderInDb(order_id='1',
                     products=OrderProductsInDb(total=2,
                                                products=['product-1',
                                                          'product-2'])).dict(),
           OrderInDb(order_id='2',
                     user='user-id',
                     products=OrderProductsInDb(total=2,
                                                products=['product-3', 'product-4'])).dict(),
           OrderInDb(order_id='3',
                     user='user-id',
                     products=OrderProductsInDb(total=1, products=['product-5'])).dict()
           ]


@pytest.fixture
def db(existing_orders):
    mocked_db = create_autospec(Db, spec_set=True)
    with patch('orders.v1.orders.api.get_db', return_value=mocked_db.return_value):
        mocked_db.return_value.orders.all.return_value = existing_orders
        yield mocked_db.return_value


@pytest.fixture
def user():
    from orders.authorization import get_current_user
    _user = UserInDb(user_id='user-id', username='user')

    def get_user_mock():
        return _user

    api.dependency_overrides[get_current_user] = get_user_mock
    yield _user
    del api.dependency_overrides[get_current_user]


def test_create_order_for_anonymous_user(db):
    new_order = NewOrderReq(products=['product-1'])
    resp = TestClient(api).post('/api/v1/orders', json=new_order.dict())
    db.orders.insert.assert_called_once_with(OrderInDb(products=OrderProductsInDb(total=1, **new_order.dict()),
                                                       status=OrderStatus.new,
                                                       order_id='4'))
    assert resp.json()['order_link'] == '/api/v1/orders/4'


def test_create_order_for_logged_in_user(db, user):
    new_order = NewOrderReq(products=['product-1'])
    with patch('orders.v1.orders.api.get_current_user', return_value=user):
        resp = TestClient(api).post('/api/v1/orders',
                                    headers={"authorization": 'bearer token'},
                                    json=new_order.dict())
    order_in_db = OrderInDb(user=user.user_id,
                            products=OrderProductsInDb(total=1, **new_order.dict()),
                            status=OrderStatus.new,
                            order_id='4')
    db.orders.insert.assert_called_once_with(order_in_db.dict())
    assert resp.json()['order_link'] == '/api/v1/orders/4'


def test_get_all_orders_of_user(db, existing_orders, user):
    db.orders.search.return_value = existing_orders[1:]
    resp = TestClient(api).get('/api/v1/orders', headers={"authorization": 'bearer token'})
    assert Orders(**resp.json()) == Orders(total=2,
                                           orders=[
                                               Order(user=user.user_id,
                                                     order_id='2',
                                                     products=OrderProducts(total=2,
                                                                            products=['/api/v1/products/product-3',
                                                                                      '/api/v1/products/product-4']),
                                                     links=OrderLinks(self='/api/v1/orders/2')),
                                               Order(user=user.user_id,
                                                     order_id='3',
                                                     products=OrderProducts(total=1,
                                                                            products=['/api/v1/products/product-5']),
                                                     links=OrderLinks(self='/api/v1/orders/3'))
                                           ],
                                           links=OrdersLinks(self='/api/v1/orders?page=1'),
                                           count=2)


def test_get_all_orders_of_user_paginated(db, existing_orders, user):
    db.orders.search.return_value = existing_orders[1:]
    with patch('orders.v1.orders.api.get_page_size', return_value=1):
        resp = TestClient(api).get('/api/v1/orders?page=1', headers={"authorization": 'bearer token'})
    assert Orders(**resp.json()) == Orders(total=2,
                                           orders=[
                                               Order(user=user.user_id,
                                                     order_id='2',
                                                     products=OrderProducts(total=2,
                                                                            products=['/api/v1/products/product-3',
                                                                                      '/api/v1/products/product-4']),
                                                     links=OrderLinks(self='/api/v1/orders/2'))],
                                           links=OrdersLinks(self='/api/v1/orders?page=1',
                                                             next='/api/v1/orders?page=2'),
                                           count=1)
    with patch('orders.v1.orders.api.get_page_size', return_value=1):
        resp = TestClient(api).get('/api/v1/orders?page=2', headers={"authorization": 'bearer token'})
    assert Orders(**resp.json()) == Orders(total=2,
                                           orders=[
                                               Order(user=user.user_id,
                                                     order_id='3',
                                                     products=OrderProducts(total=1,
                                                                            products=['/api/v1/products/product-5']),
                                                     links=OrderLinks(self='/api/v1/orders/3'))],
                                           links=OrdersLinks(self='/api/v1/orders?page=2',
                                                             previous='/api/v1/orders?page=1'),
                                           count=1)


def test_get_orders_of_user_extended_products(db, existing_orders, user):
    db.orders.search.return_value = existing_orders[1:]
    db.products.search.return_value = [{'product_id': f'product-{id_}',
                                        'description_': f'some description {id_}',
                                        'images': [f'image {id_}']
                                        } for id_ in range(3, 6)]
    resp = TestClient(api).get('/api/v1/orders?include=products.products',
                               headers={"authorization": 'bearer token'})
    expected = Orders(total=2,
                      orders=[
                          Order(user=user.user_id,
                                order_id='2',
                                products=OrderProducts(total=2,
                                                       products=[
                                                           Product(links=ProductLinks(self='/api/v1/products/product-3'),
                                                                   description_='some description 3',
                                                                   images=['image 3']),
                                                           Product(links=ProductLinks(self='/api/v1/products/product-4'),
                                                                   description_='some description 4',
                                                                   images=['image 4'])]),
                                links=OrderLinks(self='/api/v1/orders/2')),
                          Order(user=user.user_id,
                                order_id='3',
                                products=OrderProducts(total=1,
                                                       products=[
                                                           Product(links=ProductLinks(self='/api/v1/products/product-5'),
                                                                   description_='some description 5',
                                                                   images=['image 5'])]),
                                links=OrderLinks(self='/api/v1/orders/3'))],
                      links=OrdersLinks(self='/api/v1/orders?page=1'),
                      count=2)
    assert Orders(**resp.json()) == expected


def test_update_order(db, existing_orders, user):
    db.orders.search.return_value = [existing_orders[0]]
    order_update = copy.deepcopy(existing_orders[0])
    order_update['user'] = 'user-id-2'
    TestClient(api).put('/api/v1/orders/1',
                        headers={"authorization": 'bearer token'},
                        json=order_update)
    db.orders.update.assert_called_once_with({'user': 'user-id-2',
                                              'order_id': '1',
                                              'products': existing_orders[0]['products'],
                                              'status': OrderStatus.new,
                                              'links': None},
                                             ANY)


def test_update_order_not_found(existing_orders, db, user):
    db.orders.search.return_value = []
    order_update = copy.deepcopy(existing_orders[0])
    order_update['user'] = 'user-id-2'
    resp = TestClient(api).put('/api/v1/orders/1',
                               headers={"authorization": 'bearer token'},
                               json=order_update)
    assert resp.status_code == status.HTTP_404_NOT_FOUND


def test_delete_order(db, user):
    with patch('orders.v1.orders.api.Query') as query_mock:
        TestClient(api).delete('/api/v1/orders/1')
        query_mock.return_value.order_id.__eq__.assert_called_once_with('1')
        query_mock.return_value.user_id.__eq__.assert_called_once_with('user-id')
    db.orders.remove.assert_called_once()


def test_delete_order_not_found(db, user):
    db.orders.remove.return_value = []
    resp = TestClient(api).delete('/api/v1/orders/1')
    assert resp.status_code == status.HTTP_404_NOT_FOUND
