from starlette.testclient import TestClient
from mock import patch, create_autospec, ANY
import pytest

from orders.v1.products.models import Product, ProductLinks, ProductInDb, Products, ProductsLinks
from orders.api import api
from orders.database import Db


@pytest.fixture
def products():
    yield [ProductInDb(description_=f'description {id_}',
                       images=[f'image {id_}'],
                       product_id=str(id_)).dict() for id_ in range(1, 3)]


@pytest.fixture
def db(products):
    mocked_db = create_autospec(Db, spec_set=True)
    with patch('orders.v1.products.api.get_db', return_value=mocked_db.return_value):
        mocked_db.return_value.products.all.return_value = products
        yield mocked_db.return_value


def test_get_products(db):
    resp = TestClient(api).get('/api/v1/products')
    expected = Products(total=2,
                        count=2,
                        items=[Product(description_='description 1',
                                       images=['image 1'],
                                       links=ProductLinks(self='/api/v1/products/1')),
                               Product(description_='description 2',
                                       images=['image 2'],
                                       links=ProductLinks(self='/api/v1/products/2'))
                               ],
                        links=ProductsLinks(self='/api/v1/products?page=1'))
    assert Products(**resp.json()) == expected


def test_get_products_paginated(db):
    with patch('orders.v1.products.api.get_page_size', return_value=1):
        resp = TestClient(api).get('/api/v1/products')
    expected = Products(total=2,
                        count=1,
                        items=[Product(description_='description 1',
                                       images=['image 1'],
                                       links=ProductLinks(self='/api/v1/products/1'))
                               ],
                        links=ProductsLinks(self='/api/v1/products?page=1',
                                            next='/api/v1/products?page=2'))
    assert Products(**resp.json()) == expected
    with patch('orders.v1.products.api.get_page_size', return_value=1):
        resp = TestClient(api).get('/api/v1/products?page=2')
    expected = Products(total=2,
                        count=1,
                        items=[Product(description_='description 2',
                                       images=['image 2'],
                                       links=ProductLinks(self='/api/v1/products/2'))
                               ],
                        links=ProductsLinks(self='/api/v1/products?page=2',
                                            previous='/api/v1/products?page=1'))
    assert Products(**resp.json()) == expected
