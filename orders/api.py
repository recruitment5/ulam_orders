from fastapi import FastAPI
import uvicorn

from orders.v1.orders.api import orders_api
from orders.v1.products.api import products_api

api = FastAPI()
api.include_router(orders_api)
api.include_router(products_api)


if __name__ == "__main__":
    uvicorn.run("api:api", host="0.0.0.0", port=6100, log_level="info")
