def get_offsets(num_all_orders_in_db: int, page: int, page_size) -> (int, int):
    start = (page - 1) * page_size
    end = page * page_size if page * page_size < num_all_orders_in_db else None
    return start, end
