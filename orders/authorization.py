import jwt
from fastapi import Depends, APIRouter, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext
from tinydb import Query

from .database import get_db
from .v1.user_model import UserInDb


# to get a string like this run:
# openssl rand -hex 32
SECRET_KEY = 'c86691ff1145b625ad806e8117fa6bcbf91d1fde89afbd7350203c7ca8dce629'
ALGORITHM = 'HS256'
ACCESS_TOKEN_EXPIRE_MINUTES = 7 * 24 * 60

api_root = '/api/v1'


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl=api_root + '/token')

skladak = APIRouter()


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def _get_user(username: str) -> UserInDb:
    db = get_db()
    user = db.users.get(Query().username == username)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return user


async def get_current_user(token: str = Depends(oauth2_scheme)) -> UserInDb:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
    except jwt.PyJWTError:
        raise credentials_exception
    user = _get_user(username)
    if user is None:
        raise credentials_exception
    return user
