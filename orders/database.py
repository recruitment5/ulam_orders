import attr

from tinydb import TinyDB
from tinydb.table import Table


@attr.s
class Db:
    _db = attr.ib(converter=TinyDB)
    _orders: Table = None
    _products: Table = None
    _users: Table = None

    def __attrs_post_init__(self):
        self._orders = self._db.table('orders')
        self._products = self._db.table('products')
        self._users = self._db.table('users')

    @property
    def orders(self):
        return self._orders

    @property
    def products(self):
        return self._products

    @property
    def users(self):
        return self._users


_db = None


def get_db() -> Db:
    global _db
    if not _db:
        _db = Db('db.json')
    return _db


def get_next_order_id(db):
    orders_all = db.orders.all()
    if orders_all:
        order_id = str(int(orders_all[-1]['order_id']) + 1)
    else:
        order_id = '1'
    return order_id
