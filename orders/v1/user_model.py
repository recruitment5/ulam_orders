from pydantic import BaseModel


class UserInDb(BaseModel):
    user_id: str
    username: str
