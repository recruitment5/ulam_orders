from enum import Enum
from typing import List, Dict

from fastapi import APIRouter, Header, Depends, HTTPException, status
from tinydb import Query

from orders.v1 import api_root_path
from orders.utils import get_offsets
from .models import (NewOrderReq, NewOrderResp, OrderInDb, OrderProductsInDb, Orders, Order,
                     OrderLinks, OrdersLinks)
from .utils import include_products_in_order, build_order_url
from orders.v1.user_model import UserInDb
from orders.database import get_db, get_next_order_id
from orders.authorization import get_current_user
from orders.v1.products.utils import build_product_url

orders_api = APIRouter()


@orders_api.post(f'{api_root_path}/orders', response_model=NewOrderResp)
async def create_order(new_order: NewOrderReq, authorization: str = Header('')):
    db = get_db()
    # this wouldn't be done if used with normal DB
    order_id = get_next_order_id(db)
    order_to_store = OrderInDb(products=OrderProductsInDb(total=len(new_order.products), **new_order.dict()),
                               order_id=order_id)
    if authorization:
        user = await get_current_user(authorization)
        order_to_store.user = user.user_id
    db.orders.insert(order_to_store.dict())
    return NewOrderResp(order_link=build_order_url(order_id))


# it's here only to avoid UT's adjustments. It wouldn't be here in production code
def get_page_size():
    return 10


class IncludeOptions(Enum):
    products = 'products.products'


@orders_api.get(f'{api_root_path}/orders', response_model=Orders)
async def get_user_orders(page: str = '1',
                          include: IncludeOptions = None,
                          user: UserInDb = Depends(get_current_user)):
    db = get_db()
    # tinydb doesn't provide a way to limit number of returned documents
    orders_in_db = db.orders.search(Query().user_id == user.user_id)
    page = int(page)
    start, end = get_offsets(len(orders_in_db), page, get_page_size())
    resp, all_products_ids = _build_response(orders_in_db[start:end])
    if include == IncludeOptions.products:
        products = db.products.search(Query().product_id.test(lambda id_: id_ in all_products_ids.values()))
        for order in resp.orders:
            include_products_in_order(order, all_products_ids[order.order_id], products)
    _build_order_links(end, page, resp)
    resp.total = len(orders_in_db)
    resp.count = len(resp.orders)
    return resp


def _build_order_links(end, page, resp):
    previous_page = f'/api/v1/orders?page={page - 1}' if page > 1 else ''
    next_page = f'/api/v1/orders?page={page + 1}' if end is not None else ''
    resp.links = OrdersLinks(self=f'/api/v1/orders?page={page}',
                             next=next_page,
                             previous=previous_page)


def _build_response(orders_in_db: List[Dict]) -> (Orders, dict):
    resp = Orders()
    all_products_ids = {}
    for order_in_db in orders_in_db:
        order = Order(**order_in_db,
                      links=OrderLinks(self=build_order_url(order_in_db["order_id"])))
        products_ids = order.products.products
        all_products_ids[order.order_id] = products_ids
        order.products.products = [build_product_url(product_id) for product_id in products_ids]
        resp.orders.append(order)
    return resp, all_products_ids


@orders_api.put(f'{api_root_path}/orders/' + '{order_id}')
async def update_order(updated_order: Order, order_id: str, user: UserInDb = Depends(get_current_user)):
    db = get_db()
    order_query = Query()
    query = (order_query.order_id == order_id) & (order_query.user_id == user.user_id)
    matching_order = db.orders.search(query)
    if not matching_order:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    db.orders.update(updated_order, query)


@orders_api.delete(f'{api_root_path}/orders/' + '{order_id}')
async def delete_order(order_id: str, user=Depends(get_current_user)):
    query = Query()
    db = get_db()
    documents = db.orders.remove((query.order_id == order_id) & (query.user_id == user.user_id))
    if not documents:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
