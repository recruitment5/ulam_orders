from typing import List, Dict

from .models import Order
from ..products.models import ProductLinks, Product
from orders.v1 import api_root_path


def include_products_in_order(order: Order, order_products_ids: List[str], products: List[Dict]):
    order_products: List[Product] = []
    for product in products:
        if product['product_id'] in order_products_ids:
            order_products.append(Product(**product,
                                          links=ProductLinks(
                                              self=f'{api_root_path}/products/{product["product_id"]}')))
    order.products.products = order_products


def build_order_url(order_id: str) -> str:
    return f'{api_root_path}/orders/{order_id}'
