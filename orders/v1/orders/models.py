from enum import Enum
from typing import List, Union

from pydantic import BaseModel

from ..products.models import Product


class ProductInOrder(BaseModel):
    link: str


class NewOrderReq(BaseModel):
    products: List[str]


class NewOrderResp(BaseModel):
    order_link: str


class OrderStatus(str, Enum):
    new = 'new'


class OrderProductsInDb(BaseModel):
    total: int
    products: List[str] = []


ProductsLinks = str


class OrderProducts(BaseModel):
    total: int
    products: Union[List[ProductsLinks], List[Product]] = []


class OrderBase(BaseModel):
    order_id: str = ''
    user: str = ''
    status: OrderStatus = OrderStatus.new


class OrderInDb(OrderBase):
    products: OrderProductsInDb


class OrderLinks(BaseModel):
    self: str


class Order(OrderBase):
    links: OrderLinks = None
    products: OrderProducts


class OrdersLinks(BaseModel):
    self: str
    previous: str = ''
    next: str = ''


class Orders(BaseModel):
    total: int = 0
    count: int = 0
    orders: List[Order] = []
    links: OrdersLinks = None
