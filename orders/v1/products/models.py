from typing import List

from pydantic import BaseModel


ImagePath = str


class ProductLinks(BaseModel):
    self: str


class ProductBase(BaseModel):
    description_: str
    images: List[ImagePath]


class ProductInDb(ProductBase):
    product_id: str


class Product(ProductBase):
    links: ProductLinks


class ProductsLinks(BaseModel):
    self: str
    previous: str = ''
    next: str = ''


class Products(BaseModel):
    total: int = 0
    count: int = 0
    items: List[Product] = []
    links: ProductsLinks = None
