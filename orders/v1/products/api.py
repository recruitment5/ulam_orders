from fastapi import APIRouter

from orders.v1 import api_root_path
from orders.database import get_db
from orders.utils import get_offsets
from .models import Products, Product, ProductLinks, ProductsLinks
from .utils import build_product_url

products_api = APIRouter()


# it's here only to avoid UT's adjustments. It wouldn't be here in production code
def get_page_size():
    return 10


@products_api.get(f'{api_root_path}/products', response_model=Products)
def get_products(page: str = 1):
    db = get_db()
    products_in_db = db.products.all()
    start, end = get_offsets(len(products_in_db), int(page), get_page_size())
    resp = Products()
    for product in products_in_db[start:end]:
        resp.items.append(Product(**product,
                                  links=ProductLinks(
                                      self=build_product_url(product['product_id']))))
    _build_products_links(end, int(page), resp)
    resp.total = len(products_in_db)
    resp.count = len(resp.items)
    return resp


def _build_products_links(end, page, resp):
    previous_page = f'/api/v1/products?page={page - 1}' if page > 1 else ''
    next_page = f'/api/v1/products?page={page + 1}' if end is not None else ''
    resp.links = ProductsLinks(self=f'/api/v1/products?page={page}',
                               next=next_page,
                               previous=previous_page)
