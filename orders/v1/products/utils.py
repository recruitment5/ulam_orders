from orders.v1 import api_root_path


def build_product_url(product_id: str):
    return f'{api_root_path}/products/{product_id}'
