from pathlib import Path

from setuptools import setup, find_packages

with Path('README.md').open(encoding='utf-8') as f:
    long_description = f.read()

with open('requirements/release') as f:
    release_requirements = f.read().splitlines()

with open('requirements/dev') as f:
    dev_requirements = f.read().splitlines()
dev_requirements.remove(dev_requirements[0])

setup(
    name='ulam-orders',
    version='1.0.0.dev1',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/recruitment5/ulam_orders',
    author='Artur Stepniak',
    author_email='aart.st@gmail.com',
    packages=find_packages(where='.'),
    python_requires='>=3.5',
    install_requires=release_requirements,
    extras_require={
        'dev': dev_requirements,
    }
)
