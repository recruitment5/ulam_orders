FROM python:3.8 as env
COPY requirements/release .
RUN pip install --prefix=/env -r release

FROM python:3.8
COPY --from=env /env /usr/local
COPY . /app
WORKDIR /app
RUN pip install -U .
CMD python orders/api.py
